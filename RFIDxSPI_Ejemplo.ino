//El archivo DumpInfo (en los ejemplos: Archivo/Ejemplos/MFRC522/DumpInfo  permite leer toda la tarjeta, incluyendo el ID

//El programa T11_RFID_Tutorial_Sketch permite escribir un bloque de memoria deseado en funcion de lo que necesito







//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6) y el modulo RFID-RC522

#include <SPI.h>
#include <MFRC522.h>

#define LED  PC13               //El led que viene con el bluepill
#define RST  PB0
#define MISO PA6
#define MOSI PA7
#define SCK  PA5
#define SDA  PA4

/*  
 *   Disposicion de pines: 
 *   3.3V ==> 3.3V
 *   RST  ==> Pin
 *   GND  ==> GND
 *   IRQ no se conecta
 *   MISO ==> Pin
 *   MOSI ==> Pin
 *   SCK  ==> Pin
 *   SS/SDA  ==> Pin
*/  


MFRC522 mfrc522(SDA, RST);  //Instancio la clase


int a = 0;

void setup() 
{
  pinMode(LED, OUTPUT);     //Coloco un led en la pata PC13
  pinMode(RST, OUTPUT);
  
  Serial.begin(9600);  
  
  SPI.begin();              //Inicializo el SPI
  mfrc522.PCD_Init();       //Init MFRC522
  Serial.println("Leyendo el RFID");
}

void loop() 
{
  if (mfrc522.PICC_IsNewCardPresent())
  {
    if (mfrc522.PICC_ReadCardSerial())
    {
      Serial.print ("Tag UID:");
      for (byte i = 0; i < mfrc522.uid.size; i++)
        {
          Serial.print (mfrc522.uid.uidByte[i] < 0x10 ? "0" : " ");
          Serial.print (mfrc522.uid.uidByte[i], HEX);
        }

        Serial.println();
        mfrc522.PICC_HaltA();
    }
  }
}
